const path = require('path')
const fs = require('fs')

// My modules
const logs = require('./logger')
// Assign logger module's "logger" function to constant "log"
const log = logs.logger
// Assign logger module's "httpLoger" function to constant "hlog"
// const hlog = logs.httpLogger

const audio_dir = '../public/sound'
const example_audio_dir = audio_dir + '/example'

log.debug(`Audio dir: ${audio_dir}`)
log.debug(`Example audio dir: ${example_audio_dir}`)

function read_files(dir) {
    const files_output = {}

    const files = fs.readdirSync(dir)

    files.forEach(file => {
        var filetype = path.extname(file).replace('.','')
        var cleanName = file.substring(0, file.lastIndexOf("."))
        var cleanDir = dir.replace('./public/', '')

        log.debug(`Clean name: ${cleanName}, Dir: ${dir}, File: ${file}`)
        files_output[cleanName] = {
            "filetype": filetype,
            "cleanName": cleanName,
            "cleanDir": cleanDir,
            "filename": file,
            "dir": dir,
            "cleanPath": `${cleanDir}/${file}`,
            "path": `${dir}/${file}`
        }
    })

    return files_output
}

function playSound(sound) {
    // Play a sound
    var audio = new Audio(sound)

    audio.play()
}

module.exports = {
    read_files,
    playSound
}