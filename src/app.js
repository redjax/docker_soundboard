'use strict'

// Imports
const express = require('express')
// const { get } = require('http')
// const path = require('path')
const favicon = require('serve-favicon')

// My modules
const logs = require('./modules/logger')
// Assign logger module's "logger" function to constant "log"
const log = logs.logger
// Assign logger module's "httpLoger" function to constant "hlog"
const hlog = logs.httpLogger

// Audio library
const audio = require('./modules/audio')

// Import config file in ./config/app_config.js
var config_file = require('./config/app_config')
// Config file has a confusing nested object modle. Create a variable to "flatten" the first level
var config = config_file.config
// Shortcut to config_file.config.sitevars
const sitevars = config.sitevars
const siteconfig = config.site_config
// const getNestedObject = config_file.getNestedObject
const secrets = config.secrets

log.debug(`App config print: ${JSON.stringify(config)}`)
log.debug(`App sitevars print: ${JSON.stringify(sitevars)}`)
log.debug(`App print: Secrets: ${JSON.stringify(secrets)}`)


// METHOD 1: the getNestedObject function. Uncomment "const getNestedObject" above
//  if using this method
// var favicon_path = getNestedObject(config, ['site_config', 'favicon_path'])
//
// METHOD 2: access the favicon_path config variable directly.
//  Comment getNestedObject if using this method
var favicon_path = siteconfig.favicon_path

log.debug(`App debug: favicon path: ${favicon_path}`)

// Create express app, port, and host
const app = express()
// Tell app to use logger.httpLogger middleware
app.use(hlog)

// const port = 8080
const port = process.env.NODE_PORT

const host = '0.0.0.0'

// set view engine
const view_engine = 'ejs'
app.set('view engine', view_engine)

// Set static file dir to public/
const static_dir = express.static('public')

// Log NODE_ENV to console if NODE_ENV is not 'production'

// App
const router = express.Router()

// Routes

// index page
router.get('/', (req,res, next) => {

    // When page loads, renders a paragraph with <%= tagline %>
    var tagline = "Oh, Kyle 🙄";

    const load_sounds = audio.read_files('./public/sound/example')
    const sounds = load_sounds

    log.debug(`Sound dictionary: ${JSON.stringify(sounds)}`)
    log.debug(`Git url: ${secrets.secret_sitevars.git_url}`)

    // Render pages/index.ejs
    res.status(200).render('pages/index', {
        // pass sitevars to all pages
        sitevars,
        tagline: tagline,
        sounds,
        secrets
    })
})


// Setup router
app.use('/', router)
// Set static directory to './public'
app.use(static_dir)
// Set favicon
app.use(favicon(favicon_path))

// Tell app to use error logging and handling functions
app.use(logErrors)
app.use(errorHandler)

function logErrors(err, req, res, next) {
    // Log errors to console
    console.error(err.stack)
    next(err)
}

function errorHandler (err, req, res, next) {
    // When page cannot resolve, send error message
    res.status(500).send('Error 500')
}

// Only print debug info to console if in development environment
if (process.env.NODE_ENV !== 'production') {
    log.info(`NODE_ENV: ${process.env.NODE_ENV}`)
    log.debug(`.env host: ${host}`)
    log.debug(`.env port: ${port}`)
    log.debug(`view engine: ${view_engine}`)
    log.debug(`sitevars: ${JSON.stringify(sitevars)}`)
    log.debug(`Log file: ${logs.opts.file.filename}`)

    // Print newline on dev to separate logs
    console.log(`\n`)

} else if (process.env.NODE_ENV == 'production') {
    log.info(`NODE_ENV: ${process.env.NODE_ENV}`)
}

// Run server
app.listen(process.env.NODE_PORT, host || 8080, host)
log.info(`Server running in Docker on http://${host}:${port}`)
