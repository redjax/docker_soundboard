# Base image
FROM node:16-alpine as base
# FROM node:16-bullseye as base
# FROM node:16 as base

# Set working directory in the container
WORKDIR /src
# COPY package.json and package-lock.json into container
COPY src/package*.json /

# Expose container app runs on
# EXPOSE 8080

## Production stage
FROM base as production
ENV NODE_ENV=production
# Install packages first to take advantage of buildcache
# Clear npm cache for a smaller docker image
RUN npm ci --production && npm cache clean --force
# COPY ./src /src, chown src to node:node
COPY ./src /
RUN chown node:node ./
USER node
CMD ["node", "start"]

## Dev stage
FROM base as dev
ENV NODE_ENV=development
# Install packages first to take advantage of buildcache
# Clear npm cache for a smaller docker image
RUN npm i -g nodemon --development && npm i --development && npm cache clean --force
COPY ./src /
RUN chown node:node ./
USER node
CMD ["nodemon", "app.js"]
