## Dockerized Node, serves a "soundboard" webpage.

### Usage:

- Run initial_setup.sh
- Edit .end file
- Put your sound files in src/public/sound
- Edit app.js to use src/pulic/sound instead of src/public/sound/example (CTRL+F if you can't find it)
- Run $> ./dc_rebuild.sh
- Run docker-compose up
- Navigate to http://**server-ip**:**port from .env file**
  - i.e.: http://localhost:8080
